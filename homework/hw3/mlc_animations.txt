Follow these steps to setup ffmpeg to work with matplotlib on MLC machines so that you can save movie files:

1. make the directory /home/userid/bin:

$ mkdir /home/userid/bin

where "userid" is replaced appropriately.

2. cp ffmpeg to this directory:

$ cp ffmpeg /home/userid/bin/

where again, "userid" is replace appropriately.

3. Check that ffmpeg is now in your path:

$ which ffmpeg

4. If it is not, in the /home/userid/bin/ directory:

$ chmod a+x ffmpeg
